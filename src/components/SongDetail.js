import React from 'react';
import {connect} from 'react-redux';
import { getDefaultNormalizer } from '@testing-library/react';


const SongDetail = ({song})=>{
    if(!song){
        return <div>Select a song for more info</div>
    }
    return (
    <div>
        <h3>Details for: </h3>
        <p>
            Title: {song.title}
            <br/>
            Duration:{song.duration}
        </p>
        
    </div>)
};


const mapStateToProps = (state) =>{
   return {song: state.selectedSong}
};
export default connect(mapStateToProps)(SongDetail);